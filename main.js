import App from './App'
import Api from './api/api.js'
// #ifndef VUE3
import Vue from 'vue'
import store from 'store/index.js'

import request from "./api/request.js"
import uniApi from "./api/uniAPI.js"
Vue.config.productionTip = false
Vue.prototype.Api= Api
Vue.prototype.$store = store
//注册全局组件
Vue.prototype.Request=request
Vue.prototype.Urls=uniApi
App.mpType = 'app'
const app = new Vue({
	store,
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif