import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
const store = new Vuex.Store({
	state: {
		
		userInfo: {},
		codeList:[],
		currWallet:{},
		ETH:[],
		TRX:[],
		isMsg:false,
		msgList:[]
	},
	mutations: {
		setUserInfo(state, provider) {
			state.userInfo = provider;
			uni.setStorageSync( 'userInfo',provider)
		},
		
		setCodeList(state,provider){
			state.codeList=provider
			uni.setStorage({ //缓存用户登陆状态
				key: 'codeLsit',
				data: provider
			})
		},
		
		setcurrWallet(state,provider){
			state.currWallet=provider
			uni.setStorage({ //缓存用户登陆状态
				key: 'currWallet',
				data: provider
			})
		},
		
		setETH(state,provider){
			state.ETH=provider
			uni.setStorage({ //缓存用户登陆状态
				key: 'ETH',
				data: provider
			})
		},
		setTRX(state,provider){
			state.TRX=provider
			uni.setStorage({ //缓存用户登陆状态
				key: 'TRX',
				data: provider
			})
		},
		setMsg(state,provider){
			state.isMsg=provider
		},
		setMsgList(state,provider){
			state.msgList=provider
		}
		
		
		
		
	},
	actions: {

	}
})

export default store
