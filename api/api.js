
function Api() {}

//跳转到底部菜单
Api.prototype.tab_to = function(url) {
	console.log("进来了")
	if(!url){this.toast('跳转路径为空');return;}
    uni.switchTab({
        url: url,
    })
};

/**
 * 跳转到单个页面
 * @param {Object} url 页面路径
 * @param {Object} delay 延迟跳转的时间 单位毫秒
 * @param {Object} ani 动画 
 */
Api.prototype.go_to = function(url, delay,ani='slide-in-right') {
	
	if(!url) {this.toast('跳转路径为空'); return; }
	if(delay){
		setTimeout(function(){
			uni.navigateTo({
			    url: url,
				animationType:ani,
				animationDuration: 200
			})
		}, delay)
	}else{
		uni.navigateTo({
		    url: url,
			animationType:ani,
			animationDuration: 200
		})
	}
}

/**
 * 返回上一个页面
 * @param {Object} num    返回上一级的层数
 * @param {Object} delay  
 */
Api.prototype.back = function(num = 1, delay) {
	var pages = getCurrentPages();//当前页
	if(pages.length > 1){
		if(delay){
			setTimeout(function(){
				uni.navigateBack({
					delta: num > 1 ? num : 1
				})
			}, delay)
		}else{
			uni.navigateBack({
				delta: num > 1 ? num : 1
			})
		}
	}else{
		if(delay){
			setTimeout(function(){
				uni.navigateTo({
				    url: '/pages/index/index',
				})
			}, delay)
		}else{
			uni.navigateTo({
			    url: '/pages/index/index',
			})
		}
	}
}

//关闭当前页面，跳转某个页面
Api.prototype.close_to = function(url, delay) {
	if(!url){this.toast('跳转路径为空');return;}
	if(delay){
		setTimeout(()=>{
			uni.redirectTo({
				url:url
			})
		},delay)
	}else{
		uni.redirectTo({
			url:url
		})
	}
	
}
//关闭所有页面，跳转某个页面
Api.prototype.closeall_to = function(url) {
	if(!url){this.toast('跳转路径为空');return;}
	uni.reLaunch({
		url:url
	})
}



Api.prototype.closeAllTo = function(url) {
	this.closeall_to(url)
}
//存储


// 模态框
Api.prototype.showModal = function(title,content,callback) {
	// if (!content) { return; }
	// #ifdef APP-PLUS
	uni.showModal({
	    title: title ? title : '',
	    content: content ? content : '',
		cancelText:'确认',
		confirmText:'取消',
		confirmColor:'#206f41',
	    success: function (res) {			
	        if (res.cancel) {
	           callback();
	        }else if (res.confirm) {
				
			}
	    }
	});
	// #endif
	
	// #ifndef APP-PLUS
	uni.showModal({
	    title: title ? title : '',
	    content: content ? content : '',
		cancelText:'取消',
		confirmText:'确认',
		confirmColor:'#206f41',
	    success: function (res) {			
	        if (res.confirm) {
	           callback();
	        }else if (res.cancel) {
				
			}
	    }
	});
	// #endif
}


// 确认框
Api.prototype.showAlert = function(title,content,callback) {
	// if (!content) { return; }
	uni.showModal({
	    title: title ? title : '',
	    content: content ? content : '',
		cancelText:'取消',
		confirmText:'确认',
		confirmColor:'#206f41',
		showCancel: false,
	    success: function (res) {			
	        if (res.confirm && callback) {
	           callback();
	        }else if (res.cancel) {
				
			}
	    }
	});
}

// 消息提醒
Api.prototype.toast = function(text, type='none', time=2500) {
    if (!text) { return; }
    this.h_loading();
    uni.showToast({
        title: text,
        icon: type,
        duration: time,
		position:'bottom'
    })
}

Api.prototype.s_loading = function(text='加载中',mark=true){
    uni.showLoading({
        mask: mark,
        title: text,
    })
}

Api.prototype.h_loading = function(){
    uni.hideLoading();
}
// #ifndef H5
//复制到剪切板
Api.prototype.copy = function(text) {
	let t = this;
    if (!text) {
        this.toast('复制内容不能为空');
        return;
    }
    uni.setClipboardData({
        data: text,
        success(res) {
            t.toast('复制成功');
        }
    })
}
// #endif
//滚动到特定位置,1:选择器，2：距离顶部距离
Api.prototype.scroll_to = function(type, text) {
    let t_data = {
        duration: 300
    };
    if (type == 1) {
        t_data.selector = text;
    } else {
        t_data.scrollTop = text;
    }
    uni.pageScrollTo(t_data)
}
//拨打电话
Api.prototype.tel = function(num) {
    if (!num) {
        this.toast('电话号码为空');
    }
    uni.makePhoneCall({
        phoneNumber: num //填写电话号码
    })
}

//预览单张图片
Api.prototype.show_img = function(url) {
	uni.previewImage({
		current: 0,
		urls: [url],
	});
}

//预览多张图片
Api.prototype.show_imgs = function(i, imgs) {
	uni.previewImage({
		current: i,
		urls: imgs,
		// longPressActions: {
		// 	itemList: ['发送给朋友', '保存图片', '收藏'],
		// 	success: function(data) {
		// 		console.log('选中了第' + (data.tapIndex + 1) + '个按钮,第' + (data.index + 1) + '张图片');
		// 	},
		// 	fail: function(err) {
		// 		console.log(err.errMsg);
		// 	}
		// }
	});
}
//下载
Api.prototype.down_file = function(url){	
	let that = this;
	that.s_loading('下载中...');
	uni.downloadFile({
	    url: url, 
	    success: (res) => {
			console.log(res)
	        if (res.statusCode === 200) {
				that.h_loading();
				uni.saveImageToPhotosAlbum({
					filePath: res.tempFilePath,
					success: function () {
						that.toast('下载成功');
						that.h_loading();
					}
				});
	        }
	    }
	});
}

//打开文档
Api.prototype.opendoc = function(src,type){
	uni.showLoading({
		title: '正在加载文件...',
		mask: true
	})
	uni.downloadFile({
		url: src,
		success: function(res) {
			uni.hideLoading()
			let filePath = res.tempFilePath;
			uni.openDocument({
				filePath: filePath,
				fileType: type?type:'pdf',
				success: function(resa) {
					console.log('打开文档成功')
				},
			});
		},
		fail() {
			uni.hideLoading()
			uni.showToast({
				icon: 'none',
				title:'文件下载失败,请重试'
			})
		}
	});
}

// 支付
Api.prototype.app_pay = function(paytype,orderInfo,callback){
	uni.requestPayment({
	    provider: paytype,
	    orderInfo: orderInfo, //微信、支付宝订单数据
	    success: function (res) {
	        console.log('success:' + JSON.stringify(res));
			callback(0);
	    },
	    fail: function (err) {
	        console.log('fail:' + JSON.stringify(err));
			callback(1);
	    }		
	});
}

export default new Api()